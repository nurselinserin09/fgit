PREFIX = /usr/local

INSTALL = install
SHELLCHECK = shellcheck

name = $(notdir $(CURDIR))

target_path = $(PREFIX)/bin/$(name)

.PHONY: test
test:
	$(CURDIR)/test.sh

.PHONY: lint
lint:
	$(SHELLCHECK) *.sh

.PHONY: install
install:
	$(INSTALL) -D fgit.sh $(target_path)

include variables.mk
