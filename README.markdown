# Folder Git

[![Build Status](https://gitlab.com/victor-engmark/fgit/badges/master/build.svg)](https://gitlab.com/victor-engmark/fgit)

Run a Git command in several repositories.

## Usage

    fgit --help

For example, to run `git pull` in all repositories in your home directory:

    fgit pull -- ~/*/.git/..

## Test

    make test

## Install

    sudo make install
